module Workflow

  class User

    def self.create_images
      icollection = Dir.entries("_images/.").sort

      print "Select which image >> "
      iselection  = gets.chomp.to_i

      selected_image = icollection[iselection]

      system("jp2a _images/#{selected_image} > _input/user/user_image.txt")
    end

  end

  class AI

    def self.create_images
      icollection = Dir.entries("_images/.").sort
      iselection  = File.read("_input/ai/ai_input.txt").to_s.to_i

      selected_image = icollection[iselection]
      system("jp2a _images/#{selected_image} > _input/ai/ai_image.txt")
    end

    # Increases iselection by one interval.
    def self.ai_reselection
      iselection = File.read("_input/ai/ai_input.txt").to_s.to_i

      iselection = iselection + 1

      open("_input/ai/ai_input.txt", "w") { |f|
        f.puts iselection
      }
    end

    def self.exit
      abort
    end

  end

  class Compare

    def self.images
      user_image = File.read("_input/user/user_image.txt").to_s
      ai_image   = File.read("_input/ai/ai_image.txt").to_i

      # Compare user and AI input
      if user_image == ai_image
        puts "The images are the same."

        sleep(1)

        Workflow::Dataset.save_data
      else
        puts "The images are not the same."

        Workflow::AI.ai_reselection
      end
    end

  end

  # Dataset operations
  class Dataset
    def.create_dataset
      ai_image   = File.readlines("_input/ai/ai_image.txt").to_i

      # Corrupted Image
      gImage = ai_imaage

      # Uncorrupted Image
      bImage = ai_image.shuffle

      # Created dataset
      saver  = File.read("_idataset/saver.txt")
      loader = File.read("_idataset/loader.txt")

      open("image_saver.rb", "w") { |f|
        f.puts saver
      }

      open("image_loader.rb", "w") { |f|
        f.puts loader
      }
    end

    # Analyses the final dataset.
    def analyze_dataset
      system("ruby image_loader.rb")
    end
  end
end